# Where do we start from here?

# Tutorials and Assignment Repository

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

* * *
PIPELINES STATUS : [![pipeline status](https://gitlab.com/Tarsvini/ppw-lab-ki/badges/master/pipeline.svg)](https://gitlab.com/Tarsvini/ppw-lab-ki/commits/master)

COVERAGE REPORT : [![coverage report](https://gitlab.com/Tarsvini/ppw-lab-ki/badges/master/coverage.svg)](https://gitlab.com/Tarsvini/ppw-lab-ki/commits/master)






# This root directory consists of:
* **requirements.txt** : Python modules list, used when we call `pip install -r requirements.txt`
* **manage.py** : The Main Program that will run your code in sub-directory lab_1, lab_2, etc
* **.gitlab-ci.yml** : Required script that will only run on __GitLab.com__. This script will do the auto-testing & auto-deployment in GitLab server.
* **Procfile** : Required script that will only run on __Herokuapp.com__
* **README.md** : This Documentation (on markdown format)